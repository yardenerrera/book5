<?php

use Illuminate\Database\Seeder;

class Book5sSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        	       DB::table('book5s')->insert([
            	[    'author' => 'Yuval noah harari',
            	    'title' => 'The Histyory of tomaroow',
            	    'created_at' => date('Y-m-d G:i:s'),
            	    'user_id' => 1,
                   ],
                [ 'author' => 'Asher Kravitz',
            	    'title' => 'Jewish Doog',
            	    'created_at' => date('Y-m-d G:i:s'),
            	    'user_id' => 1,
                ],
                [    'author' => 'shosho',
                'title' => 'Tambalolim',
                'created_at' => date('Y-m-d G:i:s'),
                'user_id' => 1,
               ]
                 ]);
        
                     }  
}
